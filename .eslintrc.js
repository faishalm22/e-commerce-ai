const isProduction = process.env.NODE_ENV === 'production'

module.exports = {
  root: true,
  env: {
    browser: false,
    es6: true,
    node: true
  },
  extends: [
    'standard-with-typescript',
    'plugin:array-func/all',
    'plugin:jest/recommended',
    'plugin:sonarjs/recommended',
    'plugin:@typescript-eslint/eslint-recommended',
    'plugin:@typescript-eslint/recommended',
  ],
  parser: '@typescript-eslint/parser',
  parserOptions: {
    project: 'tsconfig.json',
    sourceType: 'module',
  },
  plugins: [
    "@typescript-eslint",
    'array-func',
    'eslint-comments',
    'fp',
    'jest',
    'no-secrets',
    'no-use-extend-native',
    'promise',
    'shopify',
    'sonarjs',
    'unicorn',
  ],
  rules: {
    '@typescript-eslint/no-var-requires': 'off',
    '@typescript-eslint/space-before-function-paren': 'off',
    '@typescript-eslint/strict-boolean-expressions': 'off',
    '@typescript-eslint/explicit-function-return-type': 'off',
    '@typescript-eslint/return-await': ['error', 'in-try-catch'],
    '@typescript-eslint/no-unused-vars': ['error', {
      args: 'none',
      varsIgnorePattern: '_.*',
      ignoreRestSiblings: true,
    }],
    'max-len': ['error', {
      code: 105,
      ignoreRegExpLiterals: true,
      ignoreStrings: true,
      ignoreTemplateLiterals: true,
      ignoreUrls: true,
    }],
    'no-void': ['error', {
      allowAsStatement: true,
    }],
    'function-call-argument-newline': ['error', 'consistent'],
    'comma-dangle': ['error', 'always-multiline'],
    'guard-for-in': ['error'],
    'linebreak-style': ['error', 'unix'],
    'max-classes-per-file': ['error', 1],
    'newline-per-chained-call': ['error', {
      ignoreChainWithDepth: 2,
    }],
    'no-console': isProduction ? 'error' : 'off',
    'no-mixed-operators': ['error'],
    'no-unused-vars': ['error', {
      args: 'none',
      varsIgnorePattern: '_.*',
      ignoreRestSiblings: true,
    }],
    'no-var': ['error'],
    'object-shorthand': ['error'],
    'prefer-numeric-literals': ['error'],
    'prefer-rest-params': ['error'],
    'prefer-spread': ['error'],
    'prefer-arrow-callback': ['error', {
      allowNamedFunctions: true,
    }],
    'prefer-object-spread': ['error'],
    'prefer-const': ['error', {
      destructuring: 'all',
    }],
    'sort-vars': ['error'],
    'sort-imports': ['error'],
    'space-before-function-paren': ['error', {
      anonymous: 'never',
      named: 'never',
      asyncArrow: 'always',
    }],
    'switch-colon-spacing': ['error'],

    'array-func/prefer-array-from': 'off',

    'eslint-comments/no-duplicate-disable': 'error',
    'eslint-comments/no-unused-disable': 'error',
    'eslint-comments/no-unused-enable': 'error',

    'fp/no-arguments': 'error',
    'fp/no-class': 'error',
    'fp/no-loops': 'error',

    'jest/expect-expect': 'off',
    'jest/no-standalone-expect': 'off',
    'jest/prefer-expect-assertions': 'off',
    'jest/valid-expect': 'off',

    'no-secrets/no-secrets': ['error', {
      tolerance: 5,
    }],

    'no-use-extend-native/no-use-extend-native': 'error',

    'padding-line-between-statements': [
      'error',
      {
        blankLine: 'always',
        prev: [
          'cjs-export',
          'class',
          'default',
          'directive',
          'export',
          'iife',
          'try',
        ],
        next: '*',
      },
      {
        blankLine: 'always',
        prev: '*',
        next: [
          'cjs-export',
          'cjs-import',
          'class',
          'default',
          'directive',
          'export',
          'iife',
          'import',
          'try',
        ],
      },
      {
        blankLine: 'never',
        prev: [
          'directive',
          'try',
        ],
        next: [
          'directive',
          'try',
        ],
      },
      {
        blankLine: 'never',
        prev: [
          'cjs-import',
          'import',
        ],
        next: [
          'cjs-import',
          'import',
        ],
      },
      {
        blankLine: 'any',
        prev: [
          'cjs-export',
          'export',
        ],
        next: '*',
      },
      {
        blankLine: 'always',
        prev: [
          'block-like',
          'block',
          'function',
          'iife',
          'multiline-const',
          'multiline-let',
          'multiline-var',
          'try',
        ],
        next: 'return',
      },
    ],

    'promise/always-return': 'error',
    'promise/no-return-wrap': 'error',
    'promise/param-names': 'error',
    'promise/catch-or-return': 'error',
    'promise/no-native': 'off',
    'promise/no-nesting': 'warn',
    'promise/no-promise-in-callback': 'warn',
    'promise/no-callback-in-promise': 'warn',
    'promise/avoid-new': 'off',
    'promise/no-new-statics': 'error',
    'promise/no-return-in-finally': 'warn',
    'promise/valid-params': 'warn',
    'promise/prefer-await-to-then': 'warn',
    'promise/prefer-await-to-callbacks': 'warn',

    'shopify/webpack/no-unnamed-dynamic-imports': 'error',
    'shopify/prefer-early-return': 'error',
    'shopify/restrict-full-import': ['error', [
      'collect.js',
      'lazy.js',
      'lodash',
      'underscore',
      'rambda',
      'ramda',
    ]],

    'unicorn/catch-error-name': 'error',
    // 'unicorn/consistent-function-scoping': 'error', // TODO: enable
    'unicorn/custom-error-definition': 'off',
    'unicorn/error-message': 'error',
    'unicorn/escape-case': 'error',
    // 'unicorn/expiring-todo-comments': 'error',
    // 'unicorn/explicit-length-check': 'error',
    'unicorn/filename-case': ['error', {
      cases: {
        pascalCase: true,
        camelCase: true,
      },
    }],
    'unicorn/import-index': 'error',
    'unicorn/new-for-builtins': 'error',
    'unicorn/no-abusive-eslint-disable': 'error',
    'unicorn/no-array-instanceof': 'error',
    'unicorn/no-console-spaces': 'error',
    'unicorn/no-fn-reference-in-iterator': 'off',
    // 'unicorn/no-for-loop': 'error',
    'unicorn/no-hex-escape': 'error',
    'unicorn/no-keyword-prefix': 'off',
    'no-nested-ternary': 'off',
    'unicorn/no-nested-ternary': 'error',
    'unicorn/no-new-buffer': 'error',
    'unicorn/no-process-exit': 'error',
    'unicorn/no-unreadable-array-destructuring': 'error',
    'unicorn/no-unsafe-regex': 'off',
    'unicorn/no-unused-properties': 'off',
    'unicorn/no-zero-fractions': 'error',
    'unicorn/number-literal-case': 'error',
    'unicorn/prefer-add-event-listener': 'error',
    'unicorn/prefer-array-find': 'error',
    'unicorn/prefer-dataset': 'error',
    'unicorn/prefer-event-key': 'error',
    'unicorn/prefer-exponentiation-operator': 'error',
    'unicorn/prefer-flat-map': 'error',
    'unicorn/prefer-includes': 'error',
    'unicorn/prefer-modern-dom-apis': 'error',
    'unicorn/prefer-negative-index': 'error',
    'unicorn/prefer-node-append': 'error',
    'unicorn/prefer-node-remove': 'error',
    // 'unicorn/prefer-query-selector': 'error',
    'unicorn/prefer-reflect-apply': 'error',
    'unicorn/prefer-spread': 'error',
    'unicorn/prefer-starts-ends-with': 'error',
    'unicorn/prefer-string-slice': 'error',
    'unicorn/prefer-text-content': 'error',
    'unicorn/prefer-trim-start-end': 'error',
    'unicorn/prefer-type-error': 'error',
    'unicorn/prevent-abbreviations': ['error', {
      replacements: {
        btn: false,
        ctx: false,
        prop: false,
        props: false,
        ref: false,
        refs: false,
        params: false,
      },
    }],
    'unicorn/regex-shorthand': 'error',
    'unicorn/throw-new-error': 'error',
  },
  overrides: [
    {
      files: [
        '**/__tests__/*.{j,t}s?(x)',
        '**/tests/unit/**/*.spec.{j,t}s?(x)',
      ],
      env: {
        jest: true,
      },
    },
  ],
}
