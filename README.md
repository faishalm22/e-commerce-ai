# eCommerce AI

- Simple eCommerce package
  - Search by image
  - Semantic search
  - Similar images
  - Similar texts
- Based on Spotify Annoy

Supports Linux, Windows, and MacOS. Node version >= 14. Prebuilt binaries are already updated. If not available for your configuration then you need to have *cmake* in path. Then install cmake-js globally

`(sudo) apt-get -y install cmake`
`npm install -g cmake-js`

Make sure you have valid C++ compiler

Documentation comming soon 🔜