import { createFoldersByPath } from '../../src/utils'
import { rmdirSync } from 'fs'

describe('\'createFoldersByPath\' function', () => {
  it('creates folder already created', async () => {
    expect(createFoldersByPath('./eCommerceAIData'))
  })
  it('creates folder', async () => {
    expect(createFoldersByPath('./test'))
    rmdirSync('./test')
  })
})
