import { methodNotInitialized } from '../../src/utils'

describe('\'methodNotInitialized\' function', () => {
  it('throws error', async () => {
    expect(methodNotInitialized)
      .toThrowError('Method not initialized')
  })
})
