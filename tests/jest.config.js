const { compilerOptions: { paths } } = require('../tsconfig.json')
const moduleNameMapper = Object.fromEntries(Object
  .entries(paths)
  .map(([alias, [destination]]) => [
    `${alias.replace('*', '(.*)')}$`,
    `<rootDir>/${destination.slice(2).replace('/*', '/$1')}`,
  ]))

module.exports = {
  // globalTeardown: `${__dirname}/global/teardown.js`,
  // moduleNameMapper,
  name: require('../package.json').name,
  rootDir: `${__dirname.replace('/tests', '')}`,
  testEnvironment: 'node',
  testRegex: '(/test.*.(test|spec))\\.jsx?$',
  testURL: 'https://localhost',
  testTimeout: 20000,
  verbose: false,
}
