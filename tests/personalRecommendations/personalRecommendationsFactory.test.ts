
import { initializeImageSearch } from '../../src/imageSearch'
import { initializePersonalRecommendations } from '../../src/personalRecommendations'
import { initializeTextSearch } from '../../src/textSearch'

describe('\'personalRecommendationsFactory\' function', () => {
  let textSearch
  let imageSearch
  beforeAll(async () => {
    textSearch = await initializeTextSearch('./eCommerceAIData')
    imageSearch = await initializeImageSearch('./eCommerceAIData')
  })

  it('returns personalRecommednations function function', async () => {
    jest.setTimeout(40000)
    await expect(initializePersonalRecommendations('personalRecommendations', [textSearch, imageSearch]))
      .resolves
  })
  it('calls textSearch', async () => {
    jest.setTimeout(40000)
    const personalRecommendationsEngine = await initializePersonalRecommendations(
      'personalRecommendations',
      [textSearch, imageSearch],
    )
    await expect(personalRecommendationsEngine([
      { type: 'image', data: 'https://raw.githubusercontent.com/traggo/logo/master/logo.png' },
      { type: 'text', data: 'White t-shirt' },
    ]))
      .resolves
  })
})
