import { getConfig, setConfig } from '../../src/config'

describe('config management', () => {
  it('creates annoy index with urls', async () => {
    setConfig('errorLanguage', 'lt')
    expect(getConfig('errorLanguage')).toBe('lt')
  })
})
