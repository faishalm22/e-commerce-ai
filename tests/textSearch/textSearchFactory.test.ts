import { initializeTextSearch, textSearchFactory } from '../../src/textSearch'

describe('\'textSearchFactory\' function', () => {
  it('returns textSearch function', async () => {
    jest.setTimeout(40000)
    await initializeTextSearch('./eCommerceAIData')
    await expect(textSearchFactory('testTextSearch'))
      .resolves
  })
  it('calls textSearch', async () => {
    jest.setTimeout(40000)
    await initializeTextSearch('./eCommerceAIData')
    const search = await textSearchFactory('testTextSearch')
    await expect(search('test'))
      .resolves
  })
})
