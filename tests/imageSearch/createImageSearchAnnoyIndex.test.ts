import { imageSearch } from '../../src'
const { initializeImageSearch, createImageSearchAnnoyIndex } = imageSearch

describe('\'createImageSearchAnnoyIndex\' function', () => {
  it('creates annoy index with urls', async () => {
    jest.setTimeout(40000)
    await initializeImageSearch('./eCommerceAIData')
    await expect(createImageSearchAnnoyIndex([{ id: 1, url: 'test' }], 'testImageSearch'))
      .resolves
  })
})
