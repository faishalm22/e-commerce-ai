import { fetchImage } from '../../src/utils'
import { transformFetchedImageForInfer } from '../../src/imageSearch'

describe('\'transformFetchedImageForInfer\' feature', () => {
  it('transforms fetched image for infer', async () => {
    const image = await fetchImage('https://raw.githubusercontent.com/traggo/logo/master/logo.png')
    await expect(transformFetchedImageForInfer([image]))
      .resolves
  })
})
