import { fetchImage } from '../../src/utils'
import { readImage } from '../../src/imageSearch'

describe('\'readImage\' function', () => {
  it('transforms image to tensor', async () => {
    const imageResponse = await fetchImage('https://raw.githubusercontent.com/traggo/logo/master/logo.png')
    await expect(readImage(await imageResponse.arrayBuffer() as Buffer))
      .resolves
  })
})
