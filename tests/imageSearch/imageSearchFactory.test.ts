import { imageSearchFactory, initializeImageSearch } from '../../src/imageSearch'

describe('\'imageSearchFactory\' function', () => {
  it('returns imageSearch', async () => {
    jest.setTimeout(40000)
    await initializeImageSearch('./eCommerceAIData')
    await expect(imageSearchFactory('testImageSearch'))
      .resolves
  })
  it('calls imageSearch', async () => {
    jest.setTimeout(40000)
    await initializeImageSearch('./eCommerceAIData')
    const search = await imageSearchFactory('testImageSearch')
    await expect(search('https://raw.githubusercontent.com/traggo/logo/master/logo.png'))
      .resolves
  })
})
