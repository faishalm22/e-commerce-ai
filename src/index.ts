import * as imageSearch from './imageSearch'
import * as textSearch from './textSearch'

export {
  textSearch,
  imageSearch,
}
