import * as mobilenet from '@tensorflow-models/mobilenet'
import {
  IEmbeddingsGetter,
  IEmbeddingsGetterByBufferFactory,
  ISearch,
  ISearchItem,
} from './types'
import { createFoldersByPath, fetchImage, methodNotInitialized } from '../utils'
import { promises } from 'fs'
const sharp = require('sharp')
const FileType = require('file-type')
const Annoy = require('annoy')
const tfnode = require('@tensorflow/tfjs-node')
const { writeFile, readFile } = promises

const D = 1024
const searchEngines = new Map()
const annoyIndexes = new Map()

let getImageEmbeddingsByUrls: IEmbeddingsGetter = methodNotInitialized
let getImageEmbeddingsByBuffer: IEmbeddingsGetterByBufferFactory = methodNotInitialized
let dataPath = './eCommerceAIData'
let numberOfTrees = 1000
let annoyMetric = 'angular'

const readImage = async (arrayBuffer) => {
  let buffer = Buffer.from(arrayBuffer)
  const type = await FileType.fromBuffer(buffer)
  if (!type) return
  buffer = await sharp(buffer)
    .resize(256)
    .jpeg({ quality: 80 })
    .toBuffer()
  return tfnode.node.decodeImage(buffer)
}

const transformFetchedImageForInfer = async (imagesResponses) => {
  const arrayBuffers: any = await Promise.all(imagesResponses
    .filter(Boolean)
    .map(response => response.arrayBuffer()))

  return (await Promise.all(arrayBuffers
    .map(async buffer => {
      return readImage(buffer)
    }))).filter(Boolean)
}

const embeddingsGetterFactory = async (model): Promise<IEmbeddingsGetter> =>
  async (imagesUrls: string | string[]) => {
    const urls = Array.isArray(imagesUrls) ? imagesUrls : [imagesUrls]
    const batch = await Promise.all(urls
      // eslint-disable-next-line @typescript-eslint/promise-function-async
      .map(image => fetchImage(image)))
    const images = await transformFetchedImageForInfer(batch)
    return await images.map((image) => model
      .infer(image, true)
      .arraySync()[0]) as number[][]
  }

const embeddingsGetterByBufferFactory = (model): IEmbeddingsGetterByBufferFactory =>
  async (buffer: Buffer) => {
    const image = await readImage(buffer)

    return model
      .infer(image, true)
      .arraySync()
  }

const createImageSearchAnnoyIndex = async (
  searchItems: ISearchItem[],
  searchEngineKey: string,
): Promise<void> => {
  const imagesAnnoyIndex = new Annoy(D, annoyMetric)
  const path = `${dataPath}/${searchEngineKey}`
  createFoldersByPath(path)

  const itemsIds = searchItems.map(({ id }) => id)
  await writeFile(`${path}/${searchEngineKey}.json`, JSON.stringify(itemsIds))
  const chunk = 10
  const imagesUrls = searchItems.map(({ url }) => url)

  // eslint-disable-next-line fp/no-loops
  for (let index = 0; index < imagesUrls.length; index += chunk) {
    console.log(`${index}/${imagesUrls.length}`)
    const embadings = await getImageEmbeddingsByUrls(imagesUrls.slice(index, index + chunk))
    embadings.forEach((embadding, embadingIndex) => {
      imagesAnnoyIndex.addItem(index + embadingIndex, embadding)
    })
  }

  await imagesAnnoyIndex.build(numberOfTrees)
  console.log('Built')
  await imagesAnnoyIndex.save(`${dataPath}/${searchEngineKey}/${searchEngineKey}.ann`)
  console.log('Saved')
  await imagesAnnoyIndex.unload()
}

const imageSearchFactory = async (searchEngineKey: string): Promise<ISearch> => {
  const currentAnnoyIndex = new Annoy(D, annoyMetric)
  const path = `${dataPath}/${searchEngineKey}`

  createFoldersByPath(path)

  // TODO: Check .toString()
  const itemIds = JSON.parse((await readFile(`${path}/${searchEngineKey}.json`)).toString())
  await currentAnnoyIndex.load(`${dataPath}/${searchEngineKey}/${searchEngineKey}.ann`)

  const searchEngine = async (image: string | Buffer, numberOfItems = 5) => {
    const searchMatrix = typeof image === 'string'
      ? (await getImageEmbeddingsByUrls(image))
      : (await getImageEmbeddingsByBuffer(image))

    return currentAnnoyIndex.getNNsByVector(searchMatrix[0], numberOfItems)
      .map((index: number) => itemIds[index])
  }

  const annoyIndexInMemory = annoyIndexes.get(searchEngineKey)
  if (annoyIndexInMemory) {
    await annoyIndexInMemory.unload()
  }

  annoyIndexes.set(searchEngineKey, currentAnnoyIndex)
  searchEngines.set(searchEngineKey, searchEngine)
  return searchEngine
}

const getImageSearchEngine = (searchEngineKey: string): ISearch => searchEngines.get(searchEngineKey)

const initializeImageSearch = async (path: string, metric = 'angular', treesNumber = 1000): Promise<void> => {
  if (path) { dataPath = path }

  numberOfTrees = treesNumber
  annoyMetric = metric

  const model = await mobilenet.load()
  getImageEmbeddingsByUrls = await embeddingsGetterFactory(model)
  getImageEmbeddingsByBuffer = embeddingsGetterByBufferFactory(model)
}

export {
  createImageSearchAnnoyIndex,
  imageSearchFactory,
  getImageEmbeddingsByUrls,
  initializeImageSearch,
  getImageSearchEngine,
  embeddingsGetterByBufferFactory,
  embeddingsGetterFactory,
  readImage,
  transformFetchedImageForInfer,
}
