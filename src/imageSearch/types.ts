export interface ISearchItem {
  id: number
  url: string
}

export type IEmbeddingsGetter = (urls: string | string[]) => Promise<number[][]>

export type IEmbeddingsGetterByBufferFactory = (buffer: Buffer) => Promise<number[][]>

export type ISearch = (image: Buffer|string, numberOfItems?: number) => Promise<number[]>
