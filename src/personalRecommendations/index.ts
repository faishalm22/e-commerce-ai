import {
  IHistoryItem,
} from './types'

const searchEnginesStore = new Map()

const personalRecommendationsFactory = async (history: IHistoryItem[]): Promise<any> => {
  return () => {}
}

const getPersonalRecommendationshEngine = (searchEngineKey: string): any =>
  searchEnginesStore.get(searchEngineKey)

const initializePersonalRecommendations = async (
  searchEngineKey: string,
  searchEngines: any[],
): Promise<any> => {
  return searchEnginesStore.set(searchEngineKey, personalRecommendationsFactory(searchEngines))
}

export {
  initializePersonalRecommendations,
  getPersonalRecommendationshEngine,
  personalRecommendationsFactory,
}
