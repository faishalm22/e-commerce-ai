import {
  existsSync,
  mkdirSync,
} from 'fs'
import fetch, { Response } from 'node-fetch'

export const methodNotInitialized = (..._): any => {
  throw new Error('Method not initialized')
}

export const createFoldersByPath = (path: string): void => {
  if (!existsSync(path)) {
    mkdirSync(path, {
      recursive: true,
    })
  }
}

export const fetchImage = async (imageUrl: string, retryCount?: number): Promise<Response> => {
  let imageFetchRetryCount = retryCount || 0
  if (!imageUrl) return null

  try {
    // eslint-disable-next-line sonarjs/prefer-immediate-return
    const result = await fetch(imageUrl, {
      method: 'GET',
      headers: { 'Content-type': 'application/json' },
    })

    return result
  } catch (error) {
    imageFetchRetryCount++
    if (imageFetchRetryCount < 4) {
      await new Promise((resolve) => {
        setTimeout(resolve, imageFetchRetryCount * 5000)
      })
      console.log('RETRY')
      console.log(error)
      return await fetchImage(imageUrl, imageFetchRetryCount)
    } else {
      throw new Error('Failed to fetch')
    }
  }
}
