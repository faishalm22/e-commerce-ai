export interface ISearchItem {
  id: number
  text: string
}

export type ISearch = (searchPhrase: string, numberOfItems?: number) => Promise<number[]>

export type IEmbeddingsGetter = (sentences: string | string[]) => Promise<number[][]>
