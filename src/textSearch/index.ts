import * as sentenceEncoder from '@tensorflow-models/universal-sentence-encoder'
import {
  IEmbeddingsGetter,
  ISearch,
  ISearchItem,
} from './types'
import {
  createFoldersByPath,
  methodNotInitialized,
} from '../utils'
import { promises } from 'fs'
const Annoy = require('annoy')
require('@tensorflow/tfjs-node')
const { writeFile, readFile } = promises

const D = 512
const searchEngines = new Map()
const annoyIndexes = new Map()

let numberOfTrees = 1000
let annoyMetric = 'angular'
let dataPath = './eCommerceAIData'
let getTextEmbeddings: IEmbeddingsGetter = methodNotInitialized

const embeddingsGetterFactory = async (): Promise<IEmbeddingsGetter> => {
  const model = await sentenceEncoder.load()
  return async (sentences: string | string[]) => {
    const chunk = 2000
    const batches = []
    // eslint-disable-next-line fp/no-loops
    for (let index = 0; index < sentences.length; index += chunk) {
      console.log(`${index}/${sentences.length}`)
      batches.push(model.embed(sentences.slice(index, index + chunk)))
    }

    return (await Promise.all(batches)).flatMap(batch => batch.arraySync())
  }
}

const createTextSearchAnnoyIndex = async (
  searchItems: ISearchItem[],
  searchEngineKey: string,
): Promise<void> => {
  const annoyIndex = new Annoy(D, annoyMetric)
  const texts = searchItems
    .map(sentence => sentence.text.toLocaleLowerCase())

  const embeddings = (await getTextEmbeddings(texts))

  const path = `${dataPath}/${searchEngineKey}`
  createFoldersByPath(path)

  const itemsIds = searchItems.map(({ id }) => id)
  await writeFile(`${path}/${searchEngineKey}.json`, JSON.stringify(itemsIds))

  await Promise.all(embeddings.map((item, index) => {
    return annoyIndex.addItem(index, item)
  }))

  await annoyIndex.build(numberOfTrees)
  await annoyIndex.save(`${dataPath}/${searchEngineKey}/${searchEngineKey}.ann`)
  await annoyIndex.unload()
}

const textSearchFactory = async (searchEngineKey: string): Promise<ISearch> => {
  const currentAnnoyIndex = new Annoy(D, annoyMetric)
  const path = `${dataPath}/${searchEngineKey}`
  createFoldersByPath(path)

  const itemIds = JSON.parse((await readFile(`${path}/${searchEngineKey}.json`)).toString())

  await currentAnnoyIndex.load(`${dataPath}/${searchEngineKey}/${searchEngineKey}.ann`)
  const searchEngine = async (searchPhrase: string, numberOfItems = 5) => {
    const searchMatrix = (await getTextEmbeddings(searchPhrase))
    return currentAnnoyIndex.getNNsByVector(searchMatrix[0], numberOfItems)
      .map((index: number) => itemIds[index])
  }
  const annoyIndexInMemory = annoyIndexes.get(searchEngineKey)
  if (annoyIndexInMemory) {
    await annoyIndexInMemory.unload()
  }
  annoyIndexes.set(searchEngineKey, currentAnnoyIndex)
  searchEngines.set(searchEngineKey, searchEngine)
  return searchEngine
}

const getTextSearchEngine = (searchEngineKey: string): ISearch => searchEngines.get(searchEngineKey)

const initializeTextSearch = async (path: string, metric = 'angular', treesNumber = 1000): Promise<void> => {
  if (path) { dataPath = path }

  annoyMetric = metric
  numberOfTrees = treesNumber

  getTextEmbeddings = await embeddingsGetterFactory()
}

export {
  createTextSearchAnnoyIndex,
  textSearchFactory,
  getTextEmbeddings,
  initializeTextSearch,
  getTextSearchEngine,
  embeddingsGetterFactory,
}
