type ErrorNotifier = (error: Error) => void
type ConfigKeys = 'errorNotifier' | 'errorLanguage'
type ConfigValue<T> = T extends 'errorNotifier'
  ? ErrorNotifier
  : string

interface ConfigMap extends Map<string, ErrorNotifier | unknown> {
  get: <T extends ConfigKeys>(key: T) => ConfigValue<T>
}

const config: ConfigMap = new Map()

export const getConfig = <T extends ConfigKeys>(key: T): ConfigValue<T> =>
  config.get(key)

export const setConfig = <T extends ConfigKeys>(key: T, value: unknown): ConfigMap =>
  config.set(key, value)
